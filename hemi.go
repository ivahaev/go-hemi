package hemi

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"reflect"
	"strconv"
	"sync"

	"bitbucket.org/ivahaev/go-hemi/call"
	"bitbucket.org/ivahaev/go-hemi/modules"
	"bitbucket.org/ivahaev/go-hemi/modules/fs"
	"bitbucket.org/ivahaev/go-hemi/modules/path"
	"bitbucket.org/ivahaev/go-hemi/modules/timers"
	"github.com/getblank/v8worker"
	"github.com/ivahaev/go-logger"
)

var (
	vmMutex         = &sync.Mutex{}
	commandHandlers = map[string]reflect.Value{}
	requirers       = map[string]interface{}{}
	requirersMutex  = &sync.RWMutex{}
	evalSequence    int
	evalChans       = map[int]chan interface{}{}
	evalErrorsChans = map[int]chan error{}
	evalMutex       = &sync.RWMutex{}
	requiredModules = map[string]required{}
	fileReader      Reader
	disableCache    bool
)

type Worker struct {
	v8worker        *v8worker.Worker
	embeddedModules map[string]required
}

type Reader interface {
	ReadFile(string) ([]byte, error)
}

// Eval executes provided Javascript with name in closure with require provided
func (w *Worker) Eval(name, script string) (interface{}, error) {
	if script == "" {
		return nil, errors.New("Empty script")
	}
	if name == "" {
		return nil, errors.New("No name provided")
	}
	evalChan := make(chan interface{})
	errChan := make(chan error)
	evalMutex.Lock()
	sequence := evalSequence
	evalSequence++
	var strSequence = strconv.Itoa(sequence)
	evalChans[sequence] = evalChan
	evalMutex.Unlock()
	defer func() {
		evalMutex.Lock()
		delete(evalChans, sequence)
		evalMutex.Unlock()
	}()
	script = `(function() {
        var require = $requireModule.bind(this, "` + name + `");
        var result = (function() {'use strict';` + "\n" +
		script +
		`
        })();
        var message = {
            command: "$evalResult",
            sequence: ` + strSequence + `,
            arguments: [result]
        };
        $send(JSON.stringify(message));
        delete require;
        delete result;
        delete message;
    })();`
	go func() {
		err := w.v8worker.LoadWithOptions(&v8worker.ScriptOrigin{ScriptName: name, LineOffset: -3}, script)
		if err != nil {
			errChan <- err
			return
		}
	}()
	select {
	case result := <-evalChan:
		return result, nil
	case err := <-errChan:
		return nil, err
	}
	return nil, nil
}

// Eval executes provided Javascript with name in closure with require provided.
// Script must returns Promise.
func (w *Worker) EvalAsync(name, script string) (interface{}, error) {
	if script == "" {
		return nil, errors.New("Empty script")
	}
	if name == "" {
		return nil, errors.New("No name provided")
	}
	evalChan := make(chan interface{})
	errChan := make(chan error)
	evalMutex.Lock()
	sequence := evalSequence
	evalSequence++
	var strSequence = strconv.Itoa(sequence)
	evalChans[sequence] = evalChan
	evalErrorsChans[sequence] = errChan
	evalMutex.Unlock()
	defer func() {
		evalMutex.Lock()
		delete(evalChans, sequence)
		delete(evalErrorsChans, sequence)
		evalMutex.Unlock()
	}()
	script = `(function() {
        var require = $requireModule.bind(this, "` + name + `");
        var result = (function() {'use strict';` + "\n" +
		script +
		`
        })();
		result.then(res => {
			var message = {
				command: "$evalResult",
				sequence: ` + strSequence + `,
				arguments: [res]
			};
			$send(JSON.stringify(message));
		}, err => {
			var message = {
				command: "$evalResult",
				sequence: ` + strSequence + `,
				arguments: [],
				error: err + ""
			};
			$send(JSON.stringify(message));
		});
    })();`
	go func() {
		err := w.v8worker.LoadWithOptions(&v8worker.ScriptOrigin{ScriptName: name, LineOffset: -3}, script)
		if err != nil {
			errChan <- err
			return
		}
	}()
	select {
	case result := <-evalChan:
		return result, nil
	case err := <-errChan:
		return nil, err
	}
	return nil, nil
}

func (w *Worker) PushGlobalInterface(name string, value interface{}) error {
	m := message{
		Command:   "$pushGlobalInterface",
		Arguments: []interface{}{name, value},
	}
	bytes, err := json.Marshal(m)
	if err != nil {
		return err
	}
	err = w.v8worker.Send(string(bytes))
	return err
}

func (w *Worker) PushGlobalStruct(name string, value interface{}) error {
	module := w.RegisterModule(name, reflect.ValueOf(value), false)
	mes := `{"command":"$pushGlobalInterface","module":"` + name + `","arguments":["` + name + `",` + module + `]}`
	err := w.v8worker.Send(mes)
	return err
}

func (w *Worker) RegisterModule(id string, val reflect.Value, commonModule bool) string {
	// callType := reflect.TypeOf(&call.Call{})
	typ := val.Type()
	numberMethods := typ.NumMethod()
	if commonModule {
		requirersMutex.Lock()
		requiredModules[id] = required{val.Interface(), map[string]reflect.Method{}}
	} else {
		w.embeddedModules[id] = required{val.Interface(), map[string]reflect.Method{}}
	}
	methods := []string{}
	for i := 0; i < numberMethods; i++ {
		meth := typ.Method(i)
		switch meth.Name {
		case "Require", "Export":
			continue
		}
		if meth.PkgPath != "" {
			continue
		}
		// if meth.Func.Type().NumIn() < 2 ||
		// 	meth.Func.Type().In(1) != callType {
		// 	continue
		// }
		methodName := nameToJavaScript(meth.Name)
		if commonModule {
			requiredModules[id].methods[methodName] = meth
		} else {
			w.embeddedModules[id].methods[methodName] = meth
		}
		methods = append(methods, methodName)
	}
	if commonModule {
		requirersMutex.Unlock()
	}
	_result := map[string]interface{}{}
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
		val = val.Elem()
	}
	for i := 0; i < typ.NumField(); i++ {
		structField := typ.Field(i)
		if structField.PkgPath != "" {
			continue
		}
		tag := structField.Tag.Get("js")
		if tag == "-" {
			continue
		}
		var fieldName string
		if tag != "" {
			fieldName = tag
		} else {
			fieldName = nameToJavaScript(typ.Field(i).Name)
		}
		_result[fieldName] = val.FieldByName(typ.Field(i).Name).Interface()
	}
	_result = map[string]interface{}{"methods": methods, "props": _result, "$ThisIsEmbeddedModule": true}
	result, err := json.Marshal(_result)
	if err != nil {
		logger.Error("Error when marshal result", _result, err)
		return ""
	}
	return string(result)
}

type required struct {
	module  interface{}
	methods map[string]reflect.Method
}

type Requirer interface {
	Require() string
	Export() interface{}
}

type message struct {
	Module    string        `json:"module,omitempty"`
	Command   string        `json:"command,omitempty"`
	Sequence  int           `json:"sequence,omitempty"`
	Arguments []interface{} `json:"arguments,omitempty"`
	Response  interface{}   `json:"response,omitempty"`
	Error     string        `json:"error,omitempty"`
}

func (w *Worker) receiveHandler(msg string) {
	result, seq, err := w.messageCallback(msg)
	if seq == -1 { // This is callResult mark
		return
	}
	response := message{Sequence: seq}
	if err != nil {
		logger.Error(err)
		response.Error = err.Error()
		bytes, err := json.Marshal(response)
		if err != nil {
			logger.Error("Error when marshal response", err.Error())
		}
		w.v8worker.Send(string(bytes))
		return
	}
	var resArgs interface{}
	err = json.Unmarshal([]byte(result), &resArgs)
	if err != nil {
		logger.Error(err)
		response.Error = err.Error()
	} else {
		response.Response = resArgs
	}
	bytes, err := json.Marshal(response)
	if err != nil {
		logger.Error("Error when marshal response", err.Error())
	}
	w.v8worker.Send(string(bytes))
}

func (w *Worker) syncReceiveHandler(msg string) string {
	result, _, err := w.messageCallback(msg)
	if err != nil {
		logger.Error(err)
	}
	return result
}

func (w *Worker) messageCallback(msg string) (string, int, error) {
	var m message
	err := json.Unmarshal([]byte(msg), &m)
	if err != nil {
		logger.Error("Error when unmarshaling msg", msg, err)
		return "", 0, err
	}
	if m.Command == "$evalResult" {
		if m.Error == "" {
			evalMutex.RLock()
			ch, ok := evalChans[m.Sequence]
			evalMutex.RUnlock()
			if ok {
				ch <- m.Arguments[0]
			}
		} else {
			evalMutex.RLock()
			ch, ok := evalErrorsChans[m.Sequence]
			evalMutex.RUnlock()
			if ok {
				ch <- errors.New(m.Error)
			}
		}

		return "", -1, nil
	}

	if m.Module != "" && m.Command != "$moduleRequire" {
		// First looking in embedded modules
		module, ok := w.embeddedModules[m.Module]
		if !ok {
			requirersMutex.RLock()
			module, ok = requiredModules[m.Module]
			requirersMutex.RUnlock()
		}
		if !ok {
			logger.Warn("Module not required", m.Module)
			return "", 0, errors.New("Module not required")
		}
		method, ok := module.methods[m.Command]
		if !ok {
			logger.Warn("Method not found", m.Module+"."+m.Command)
			return "", 0, errors.New("Method not found")
		}
		var cbWorker = w.v8worker
		var cbReposponseSender = func(msg string) {
			err := cbWorker.Send(msg)
			if err != nil {
				logger.Error("Can't send message to worker", msg, err.Error())
			}
		}
		return call.CallMethod(m.Arguments, method.Func, module.module, cbReposponseSender), m.Sequence, nil
	}
	return w.execJsCommand(m), m.Sequence, nil
}

func (w *Worker) execJsCommand(m message) string { // добавить про ошибки
	switch m.Command {
	case "$searchModule":
		if len(m.Arguments) == 0 {
			return ""
		}
		id, ok := m.Arguments[0].(string)
		if !ok {
			return ""
		}
		id, err := modules.SearchModule(id)
		if err != nil {
			return ""
		}
		return id
	case "$resolveModuleFilePath":
		if len(m.Arguments) < 2 {
			return ""
		}
		id, ok := m.Arguments[0].(string)
		if !ok {
			logger.Error("id is not a string", id)
			return ""
		}
		parentId, ok := m.Arguments[1].(string)
		if !ok {
			logger.Error("parentId is not a string", id)
			return ""
		}
		return modules.ResolveModuleFilePath(id, parentId)
	case "$moduleRequire":
		if m.Module == "" {
			logger.Error("No ID provided when require")
			out, _ := json.Marshal(message{Error: "No ID provided when require"})
			return string(out)
		}
		for reqId := range requirers {
			if reqId != m.Module {
				continue
			}
			val := reflect.ValueOf(requirers[reqId])
			return w.RegisterModule(m.Module, val, true)
		}

		script, err := modules.Get(m.Module, disableCache)
		if err != nil {
			out, _ := json.Marshal(message{Error: err.Error()})
			return string(out)
		}

		err = w.loadModule(m.Module, script)
		if err != nil {
			out, _ := json.Marshal(message{Error: err.Error()})
			return string(out)
		}
		return "loaded"
	}
	out, _ := json.Marshal(message{Error: "Unknown command"})
	return string(out)
}

func (w *Worker) loadModule(id, script string) error {
	return w.v8worker.LoadWithOptions(&v8worker.ScriptOrigin{ScriptName: id, LineOffset: -1}, `$requiredModules ["`+id+`"] = (function() {var require = $requireModule.bind(this, "`+id+`"); var module = { exports: {} }; var exports = module.exports;
    `+
		script+
		`
    return module.exports}())`)
}

func DisableCache() {
	logger.Notice("JS Modules caching disabled")
	disableCache = true
}

func RegisterRequire(r Requirer) {
	requirersMutex.Lock()
	requirers[r.Require()] = r.Export()
	requirersMutex.Unlock()
}

// New creates new Worker and returns pointer to it
func New(initScript *string) *Worker {
	w := new(Worker)
	w.v8worker = v8worker.New(w.receiveHandler, w.syncReceiveHandler)
	w.embeddedModules = map[string]required{}

	err := w.v8worker.Load("console.js", consolePolyfill)
	if err != nil {
		panic(err)
	}
	if disableCache {
		err = w.v8worker.Load("disableCache.js", `var $disableCache = true;`)
		if err != nil {
			panic(err)
		}
	}
	err = w.v8worker.Load("cb.js", jsCb)
	if err != nil {
		panic(err)
	}
	err = w.v8worker.Load("require.js", jsRequire)
	if err != nil {
		panic(err)
	}
	err = w.PushGlobalStruct("$__timers", new(timers.Timers))
	err = w.v8worker.Load("timers.js", `
		var setTimeout = $__timers.setTimeout;
		var clearTimeout = $__timers.clearTimeout;
		var setInterval = $__timers.setInterval;
		var clearInterval = $__timers.clearInterval;`)
	if err != nil {
		panic(err)
	}
	if initScript != nil {
		err = w.v8worker.Load("init.js", *initScript)
		if err != nil {
			panic(err)
		}
	}
	return w
}

// Dispose prepares Worker to dispose by GC.
// Must be called after using Worker
func (w *Worker) Dispose() {
	w.v8worker = nil
}

// IdleNotificationDeadline
func (w *Worker) IdleNotificationDeadline(deadLineInSeconds float64) bool {
	return w.v8worker.IdleNotificationDeadline(deadLineInSeconds)
}

// LowMemoryNotification
func (w *Worker) LowMemoryNotification() {
	w.v8worker.LowMemoryNotification()
}

func (w *Worker) GetHeapStatistics() *v8worker.HeapStatistics {
	return w.v8worker.GetHeapStatistics()
}

func SetReader(r Reader) {
	fileReader = r
	fs.SetReader(r)
}

func V8Version() string {
	return v8worker.Version()
}

func init() {
	requirersMutex.Lock()
	requirers["path"] = path.GetModule()
	requirers["fs"] = fs.GetModule()

	requirersMutex.Unlock()
}

func readFile(path string) (string, error) {
	if path == "" {
		return "", errors.New("No filepath provided")
	}
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}
