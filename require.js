'use strict';

var global = this;

var $sequence = 0;
if (typeof $requiredModules === 'undefined') {
    Object.defineProperty(this, '$requiredModules', {
        value: {}, writable: true, enumerable: false, configurable: true
    });
}
if (typeof $callbacks === 'undefined') {
    Object.defineProperty(this, '$callbacks', {
        value: {}, writable: true, enumerable: false, configurable: true
    });
}

$recv(function(msg) {
  try {
      var message = JSON.parse(msg);
      if (message.command && message.command === "$pushGlobalInterface") {
          var value = JSON.parse(message.arguments[1]);
          if (message.module == message.arguments[0]) {
              value = $registerModule(message.module, value);
          }
          Object.defineProperty(this, message.arguments[0], {
              value: value, writable: true, enumerable: true, configurable: true
          });
          return
      }
      if (message.sequence && $callbacks[message.sequence]) {
          var cb = $callbacks[message.sequence];
          if (Array.isArray(message.response)) {
            cb.callback.apply(cb.context, message.response);
          } else {
              cb.callback.apply(cb.context, [message.response]);
          }
          delete $callbacks[message.sequence];
      }
  } catch (e) {
      console.error("Error when receiving message", e);
  }
});

function $requireModule (id, parentId) {
    if (id == null) {
        throw new Error("No id provided")
    }

    id = $resolveModuleFilePath(id, parentId);
    if (id == "") {
        throw new Error("No id provided")
    }

    if ($requiredModules[id]) {
        return $requiredModules[id]
    }
    var command = {
        "command": "$moduleRequire",
        "module": id
    };

    var result = $sendSync(JSON.stringify(command));
    if (result === "") {
        throw new Error("Unknown error");
    }
    var module;
    try {
        module = JSON.parse(result);
    } catch (e) {
        return $requiredModules[id]
    }

    if (module.error) {
        var newId;
        if (module.error === "file does not exist") {
            newId = $searchModule(id);
            if (newId !== "") {
                return $requireModule(newId, "");
            }
            newId = $searchModule("vendor/" + id);
            if (newId !== "") {
                return $requireModule(newId, "");
            }

            newId = $searchModule("node_modules/" + id);
            if (newId !== "") {
                return $requireModule(newId, "");
            }
        }
        if (module.error === "this is a directory") {
            newId = $searchModule(id);
            return $requireModule(newId, "");
        }
        throw new Error("Module '" + id + "' load error: " + module.error);
    }

    if (!module.$ThisIsEmbeddedModule) {
        return $requiredModules[id]
    }

    module = $registerModule(id, module);
    $requiredModules[id] = module;

    return module
}

function $registerModule(id, module) {
    var methods = module.methods;
    if (methods.length === 0) {
        module = module.props
        $requiredModules[id] = module;
        return module
    }
    module = module.props;
    var asyncRegexp = /^(\w+)Async$/;
    for (var i = 0; i < methods.length; i++) {
        var method = methods[i];
        if (asyncRegexp.test(method)) {
            $sequence++;
            module[method.match(asyncRegexp)[1]] = function(m){
                return function() {
                    var args = [];
                    var command = {
                        "module": id,
                        "command": m,
                        "sequence": $sequence,
                        "arguments": []
                    };

                    for (var i = 0; i < arguments.length; i++) {
                        if (i === arguments.length - 1) {
                            if (typeof arguments[i] === "function") {
                                $callbacks[command.sequence] = {
                                    callback: arguments[i],
                                    context: this
                                }
                            }
                        }
                        args[i] = arguments[i];
                    }
                    command.arguments = args;

                    $send(JSON.stringify(command));
                }
            }(method)
        } else {
            module[method] = function(m){
                return function() {
                    var args = [];
                    for (var i = 0; i < arguments.length; i++) {
                        args[i] = arguments[i];
                    }
                    var command = {
                        "module": id,
                        "command": m,
                        "arguments": args
                    };

                    var result = $sendSync(JSON.stringify(command));
                    return JSON.parse(result);
                }
            }(method)
        }
    }

    return module
}

function $resolveModuleFilePath(id, parentId) {
    var command = {
        "command": "$resolveModuleFilePath",
        "arguments": [id, parentId]
    };

    return $sendSync(JSON.stringify(command));
}

function $searchModule(id, parentId) {
    var command = {
        "command": "$searchModule",
        "arguments": [id, parentId]
    };

    return $sendSync(JSON.stringify(command));
}