package main

import (
	"bitbucket.org/ivahaev/go-hemi"
	"bitbucket.org/ivahaev/go-hemi/tester"
	"github.com/ivahaev/go-logger"
)

func main() {
	w := hemi.New()
	logger.Notice("Version is", hemi.V8Version())
	w.PushGlobalInterface("$data", map[string]interface{}{"propStr": "STRING", "propNum": 1, "propArr": []interface{}{"123", 321}})
	w.PushGlobalInterface("$str", "STROKE!!!!")
	err := w.PushGlobalStruct("$test", &tester.T{})
	logger.Error(err)
	result, err := w.Eval("2.js", `var a = require("test.js"); a.error("Some test error!"); var b = require("test.js"); b.error(); return "THIS IS RETURN VALUEEEEE!!!!!"`)
	if err != nil {
		logger.Error(err)
	}
	logger.Debug(result)

	result, err = w.Eval("sampleTest.js", `require("samplejs")(); return 123123123`)
	if err != nil {
		logger.Error(err)
	}
	logger.Debug(result)
}
