var fs = require('fs');
var content = fs.readFileSync("./textExample.docx", "binary");
console.info(content);
console.info(content.length);

var Docxgen = require("./docx.js");

doc = new Docxgen(content);
doc.setData( {"first_name":"Hipp",
    "last_name":"Edgar",
    "phone":"0652455478",
    "description":"New Website"
    }
) //set the templateVariables
doc.render() //apply them (replace all occurences of {first_name} by Hipp, ...)
console.error(doc.getFullText());
out = doc.getZip().generate({"type": "base64"}) //Output the document using Data-URI
fs.writeFile("./hz.docx", out, "base64");
//saveAs(out,"output.docx")
//console.log("docx: ", JSON.stringify(d));