package call

import (
	"errors"
)

var (
	ErrValueIsNil       = errors.New("Value is nil")
	ErrValueIsNotBool   = errors.New("Value is not bool")
	ErrValueIsNotNumber = errors.New("Value is not a number")
	ErrValueIsNotSlice  = errors.New("Value is not a slice")
	ErrValueIsNotObject = errors.New("Value is not an object")
)
