package call

import (
	"encoding/json"
	"errors"
	"reflect"
	"strings"
	"sync"

	"github.com/ivahaev/go-logger"
)

var (
	callbacks       = map[reflect.Value]callback{}
	callbacksLocker sync.RWMutex
)

type callback struct {
	id string
	fn func(msg string)
}

type Call struct {
	values  []*Value
	result  string
	_result []reflect.Value
	cb      func(string)
}

type callResult struct {
	Response interface{} `json:"response"`
	Error    string      `json:"error"`
}

// RemoveCallback removes provided callback from worker
func RemoveCallback(_fn interface{}) {
	var fn reflect.Value
	if v, ok := _fn.(*Value); ok {
		fn = v.v
	} else {
		fn = reflect.ValueOf(_fn)
	}
	callbacksLocker.RLock()
	cb, ok := callbacks[fn]
	callbacksLocker.RUnlock()
	if ok {
		cb.fn(`{"command":"$cb.remove","response":"` + cb.id + `"}`)
		callbacksLocker.Lock()
		delete(callbacks, fn)
		callbacksLocker.Unlock()
	}
}

// ArgumentList returns function arguments as slice
func (f *Call) ArgumentList() []interface{} {
	var result = make([]interface{}, f.ArgumentsCount())
	for i := range f.values {
		result[i] = f.values[i].Export()
	}
	return result
}

// ArgumentsCount returns number of function arguments
func (f *Call) ArgumentsCount() int {
	return len(f.values)
}

// Argument returns *Value that represents function arguments with index provided.
// If index is out of range, will return nil *Value
func (f *Call) Argument(index int) *Value {
	if len(f.values) < index-1 {
		return &Value{isUndefined: true}
	}
	return f.values[index]
}

// Arg is a short alias for Argument()
func (f *Call) Arg(index int) *Value {
	return f.Argument(index)
}

// CallMethod used for calling method with arguments provided as slice
func CallMethod(args []interface{}, method reflect.Value, _module interface{}, cb func(msg string)) string {
	f := &Call{}
	callType := reflect.TypeOf(&Call{})
	var startArgumentNumber = 1
	if _module == nil {
		startArgumentNumber = 0
	}
	var arguments []reflect.Value
	if method.Type().NumIn() >= 2 &&
		method.Type().In(startArgumentNumber) == callType {
		if _module != nil {
			module := reflect.ValueOf(_module)
			arguments = []reflect.Value{module, reflect.ValueOf(f)}
		} else {
			arguments = []reflect.Value{reflect.ValueOf(f)}
		}
		if args == nil {
			f._result = method.Call(arguments)
			f.createResult()
			return f.result
		}

		f.values = make([]*Value, len(args))
		for i := 0; i < len(args); i++ {
			val := args[i]
			value := &Value{value: args[i]}
			if val != nil {
				if strVal, ok := args[i].(string); ok && strings.HasPrefix(strVal, "$callMePlease:") {
					callbackId := strings.TrimPrefix(strVal, "$callMePlease:")
					if callbackId == "" {
						return `{"error":"Invalid arguments"}`
					}
					var fn = func(in []interface{}, remove bool) {
						cbResult := map[string]interface{}{"command": "$cb.call", "response": callbackId}
						if remove {
							cbResult["$cb.remove"] = true
						}
						cbResult["arguments"] = in
						bytes, err := json.Marshal(cbResult)
						if err != nil {
							logger.Error("Can't marshal cbResult", err.Error())
							return
						}
						cb(string(bytes))
					}
					v := reflect.ValueOf(fn)
					callbacksLocker.Lock()
					callbacks[v] = callback{id: callbackId, fn: cb}
					callbacksLocker.Unlock()
					value.fn = fn
					value.v = v
				}

			}
			f.values[i] = value
		}

		f._result = method.Call(arguments)
		f.createResult()
		return f.result
	}
	arguments = make([]reflect.Value, method.Type().NumIn())
	if _module != nil {
		arguments[0] = reflect.ValueOf(_module)
	}
	if method.Type().NumIn() == startArgumentNumber {
		f._result = method.Call(arguments)
		f.createResult()
		return f.result
	}

	if len(args) < method.Type().NumIn()-startArgumentNumber {
		return `{"error":"Not enough arguments"}`
	}

	for i := startArgumentNumber; i < method.Type().NumIn(); i++ {
		var currentArgument = method.Type().In(i)
		arg, err := getFunctionArgument(currentArgument, args[i-startArgumentNumber], cb)
		if err != nil {
			return `{"error": "` + err.Error() + `"}`
		}
		arguments[i] = arg
	}

	f._result = method.Call(arguments)
	f.createResult()
	return f.result
}

func (f *Call) createResult() {
	if len(f._result) == 0 {
		f.result = `{}`
		return
	}
	if err, ok := f._result[len(f._result)-1].Interface().(error); ok {
		f.result = `{"error":"` + err.Error() + `"}`
		return
	}
	outputValues := make([]interface{}, len(f._result))
	for i, v := range f._result {
		outputValues[i] = v.Interface()
	}

	result := callResult{}
	if len(outputValues) > 1 {
		result.Response = outputValues
	} else if len(outputValues) > 0 {
		result.Response = outputValues[0]
	} else {
		f.result = `{}`
	}
	bytes, err := json.Marshal(result)
	if err != nil {
		logger.Error("Can't marshal output", outputValues, err.Error())
		f.result = `{"error":"Can't marshal output"}`
	}
	f.result = string(bytes)
}

func getFunctionArgument(currentArgumentType reflect.Type, argument interface{}, cb func(msg string)) (reflect.Value, error) {
	_val := &Value{value: argument}
	switch currentArgumentType.Kind() {
	case reflect.Bool:
		val, err := _val.ToBoolean()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be boolean.`)
		}
		return reflect.ValueOf(val), nil
	case reflect.Int:
		val, err := _val.ToInt()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be int.`)
		}
		return reflect.ValueOf(val), nil
	case reflect.Int8:
		val, err := _val.ToInt()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be int.`)
		}
		return reflect.ValueOf(int8(val)), nil
	case reflect.Int16:
		val, err := _val.ToInt()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be int.`)
		}
		return reflect.ValueOf(int16(val)), nil
	case reflect.Int32:
		val, err := _val.ToInt()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be int.`)
		}
		return reflect.ValueOf(int32(val)), nil
	case reflect.Int64:
		val, err := _val.ToInt()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be int.`)
		}
		return reflect.ValueOf(int64(val)), nil
	case reflect.Uint:
		val, err := _val.ToInt()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be int.`)
		}
		return reflect.ValueOf(uint(val)), nil
	case reflect.Uint8:
		val, err := _val.ToInt()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be int.`)
		}
		return reflect.ValueOf(uint8(val)), nil
	case reflect.Uint16:
		val, err := _val.ToInt()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be int.`)
		}
		return reflect.ValueOf(uint16(val)), nil
	case reflect.Uint32:
		val, err := _val.ToInt()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be int.`)
		}
		return reflect.ValueOf(uint32(val)), nil
	case reflect.Uint64:
		val, err := _val.ToInt()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be int.`)
		}
		return reflect.ValueOf(uint64(val)), nil
	case reflect.Float32:
		val, err := _val.ToFloat()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be float.`)
		}
		return reflect.ValueOf(float32(val)), nil
	case reflect.Float64:
		val, err := _val.ToFloat()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be float.`)
		}
		return reflect.ValueOf(val), nil
	case reflect.Slice:
		val, err := _val.ToSlice()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be array.`)
		}
		return reflect.ValueOf(val), nil
	case reflect.Interface:
		return reflect.ValueOf(_val.Export()), nil
	case reflect.String:
		val, err := _val.ToString()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be string.`)
		}
		return reflect.ValueOf(val), nil
	case reflect.Map:
		val, err := _val.ToObject()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be object.`)
		}
		return reflect.ValueOf(val), nil
	case reflect.Func:
		strVal, err := _val.ToString()
		if err != nil {
			return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be function.`)
		}
		return MakeFunc(currentArgumentType, strVal, cb)
	}
	return reflect.Value{}, errors.New(`Unknown type of argument`)
}

func MakeFunc(currentArgumentType reflect.Type, strVal string, cb func(msg string)) (reflect.Value, error) {
	if !strings.HasPrefix(strVal, "$callMePlease:") {
		return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be function.`)
	}
	callbackId := strings.TrimPrefix(strVal, "$callMePlease:")
	if callbackId == "" {
		return reflect.Value{}, errors.New(`Invalid argument: ` + currentArgumentType.Name() + `. Must be function!`)
	}
	v := reflect.MakeFunc(currentArgumentType, func(id string) func(in []reflect.Value) []reflect.Value {
		return func(in []reflect.Value) []reflect.Value {
			cbResult := map[string]interface{}{"command": "$cb.call", "response": id}
			var cbArguments = make([]interface{}, len(in))
			for i := 0; i < len(in); i++ {
				cbArguments[i] = in[i].Interface()
			}
			cbResult["arguments"] = cbArguments
			bytes, err := json.Marshal(cbResult)
			if err != nil {
				logger.Error("Can't marshal cbResult", err.Error())
				return nil
			}
			cb(string(bytes))
			return nil
		}
	}(callbackId))

	callbacksLocker.Lock()
	callbacks[v] = callback{id: callbackId, fn: cb}
	callbacksLocker.Unlock()
	return v, nil
}
