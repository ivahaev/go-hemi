package call

import (
	"math"
	"reflect"
	"strconv"
)

func (p *Value) bool() (bool, error) {
	if p.t == reflect.Bool {
		return p.value.(bool), nil
	}
	if p.IsNull() {
		return false, nil
	}
	switch value := p.value.(type) {
	case bool:
		return value, nil
	case int, int8, int16, int32, int64:
		return 0 != reflect.ValueOf(value).Int(), nil
	case uint, uint8, uint16, uint32, uint64:
		return 0 != reflect.ValueOf(value).Uint(), nil
	case float32:
		return 0 != value, nil
	case float64:
		if math.IsNaN(value) || value == 0 {
			return false, nil
		}
		return true, nil
	case string:
		if len(value) == 0 {
			return false, nil
		}
		parsed, err := strconv.ParseBool(value)
		if err != nil {
			return true, nil
		}
		return parsed, nil
	}
	if p.IsObject() {
		return true, nil
	}
	return false, ErrValueIsNotBool
}
