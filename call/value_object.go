package call

func (p *Value) object() (map[string]interface{}, error) {
	val ,ok := p.value.(map[string]interface{})
    if !ok {
        return nil, ErrValueIsNotObject
    }

	return val, nil
}
