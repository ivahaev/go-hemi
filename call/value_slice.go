package call

func (p *Value) slice() ([]interface{}, error) {
	val, ok := p.value.([]interface{})
	if !ok {
		return nil, ErrValueIsNotSlice
	}

	return val, nil
}
