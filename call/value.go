package call

import "reflect"

// Value represents one of function argument
type Value struct {
	isUndefined bool
	value       interface{}
	t           reflect.Kind
	v           reflect.Value
	fn          func([]interface{}, bool)
}

// Export returns function argument as interface{}
func (p *Value) Export() interface{} {
	return p.value
}

func (p *Value) IsFunction() bool {
	return p.fn != nil
}

// IsNull return true if arguments is nil
func (p *Value) IsNull() bool {
	return p.value == nil
}

// IsBoolean return true if arguments is bool
func (p *Value) IsBoolean() bool {
	if p.value == nil {
		return false
	}
	_, ok := p.value.(bool)
	if ok {
		p.t = reflect.Bool
	}
	return ok
}

// IsObject return true if arguments is JS object
func (p *Value) IsObject() bool {
	if p.value == nil {
		return false
	}
	_, ok := p.value.(map[string]interface{})
	if ok {
		p.t = reflect.Map
	}
	return ok
}

// IsNumber return true if arguments is a number
func (p *Value) IsNumber() bool {
	if p.value == nil {
		return false
	}
	var kind = reflect.ValueOf(p.value).Kind()
	if kind >= 2 && kind <= 16 {
		p.t = kind
		return true
	}
	return false
}

// IsString return true if arguments is string
func (p *Value) IsString() bool {
	if p.value == nil {
		return false
	}
	_, ok := p.value.(string)
	if ok {
		p.t = reflect.String
	}
	return ok
}

// Call executes provided callback
func (p *Value) Call(in ...interface{}) {
	p.fn(in, false)
}

// CallAndRemove executes provided callback, then removes callback from worker.
func (p *Value) CallAndRemove(in ...interface{}) {
	p.fn(in, true)
}

// ToBoolean converts Value to bool with ECMA specifications
func (p *Value) ToBoolean() (bool, error) {
	return p.bool()
}

// ToFloat converts Value to float with ECMA specifications
func (p *Value) ToFloat() (float64, error) {
	return p.ToFloat64()
}

// ToFloat64 converts Value to float64 with ECMA specifications
func (p *Value) ToFloat64() (float64, error) {
	return p.float64()
}

// ToInt converts Value to int with ECMA specifications
func (p *Value) ToInt() (int, error) {
	fl, err := p.float64()
	return int(fl), err
}

// ToInteger converts Value to int with ECMA specifications
func (p *Value) ToInteger() (int, error) {
	return p.ToInt()
}

// ToObject converts Value to JS object
func (p *Value) ToObject() (map[string]interface{}, error) {
	return p.object()
}

// ToString converts Value to string with ECMA specifications
func (p *Value) ToString() (string, error) {
	return p.string(), nil
}

// ToSlice converts Value to slice
func (p *Value) ToSlice() ([]interface{}, error) {
	return p.slice()
}
