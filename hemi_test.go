package hemi

import (
	"fmt"
	"strconv"
	"testing"
	"time"
)

func TestVersion(t *testing.T) {
	println(V8Version())
}

func TestInit(t *testing.T) {
	initScript := "var v = 'test text'"
	w := New(&initScript)

	res, err := w.Eval("testInit.js", `return v`)
	if err != nil {
		t.Fatal(err)
	}
	if res.(string) != "test text" {
		t.Fatal("Test text mismatched: ", res)
	}
}

func TestTimer(t *testing.T) {
	initScript := `var firstTimerExecuted; var secondTimerExecuted;`
	w := New(&initScript)

	_, err := w.Eval("timerTest.js", `
        var now = new Date();
        var timer = setTimeout(function(){
            firstTimerExecuted = true;
        }, 2000);
        setTimeout(function(){
            secondTimerExecuted = true;
            clearTimeout(timer);
        }, 1000);
    `)
	if err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Second * 3)
	res, err := w.Eval("callbacksCount.js", `return !firstTimerExecuted && !!secondTimerExecuted;`)
	if err != nil {
		t.Fatal(err)
	}
	if !res.(bool) {
		t.Fatal("Timers failed")
	}
}

func TestDurability(t *testing.T) {
	w := New(nil)
	count := 0
	for count < 1999 {
		_, err := w.Eval("durabilityTest"+strconv.Itoa(count)+".js", `
            var firstTimerExecuted = false,
                secondTimerExecuted = false;
            var now = new Date();
            var timer = setTimeout(function(){
                firstTimerExecuted = true;
            }, 2000);
            setTimeout(function(){
                secondTimerExecuted = true;
                clearTimeout(timer);
            }, 1000);
            var moment = require('moment');
            console.log(moment().format('L'));
        `)
		if err != nil {
			t.Fatal(err)
		}
		count++
		//w.IdleNotificationDeadline(0.1)
		//time.Sleep(20 * time.Millisecond)
	}
	//w.IdleNotificationDeadline(3)
	w.LowMemoryNotification()
	time.Sleep(3000 * time.Millisecond)
	w.LowMemoryNotification()
	time.Sleep(3000 * time.Millisecond)
	s := w.GetHeapStatistics()
	fmt.Println(s.UsedHeapSize)
	time.Sleep(time.Second * 30)
}

// func TestIntervals(t *testing.T) {
// 	w, err := NewWorker("", "var intervals = 0;")
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	res, err := w.Run(`
//         var interval = setInterval(function(){
//             intervals++;
//         }, 500);
//         setTimeout(function(){
//             clearInterval(interval);
//         }, 1200);
//     `, &Options{Name: "intervalTest.js"})
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	time.Sleep(time.Second * 5)
// 	res, err = w.Run(`return intervals`, &Options{Name: "intervalChecker.js"})
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	if res.(float64) != 2 {
// 		t.Fatal("Intervals count isn't 2: ", res)
// 	}
// }

// func TestCreateObject(t *testing.T) {
// 	var fn = func(a string) string {
// 		return a + a
// 	}
// 	w, err := NewWorker("")
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	err = w.CreateGlobalObject("testFunction", fn)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	res, err := w.Run(`
//         return testFunction('Function test!');
//     `, &Options{Name: "testFunction.js"})
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	if res.(string) != "Function test!Function test!" {
// 		t.Fatal("Assertion failed ", res, "!= 'Function test!Function test!'")
// 	}
// 	err = w.CreateGlobalObject("testString", "test text")
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	res, err = w.Run(`
//         return testString;
//     `, &Options{Name: "testString.js"})
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	if res.(string) != "test text" {
// 		t.Fatal("Assertion failed ", res, "!= 'test text'")
// 	}
// 	err = w.CreateGlobalObject("testNumber", 100)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	res, err = w.Run(`
//         return testNumber;
//     `, &Options{Name: "testNumber.js"})
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	if res.(float64) != 100 {
// 		t.Fatal("Assertion failed ", res, "!= 100")
// 	}
// }

// func TestLongTaskTerminating(t *testing.T) {
// 	w, err := NewWorker("", `var $_____result = "START!";`)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	w.Run(`
//         setTimeout(function(){
//             $_____result = "After timeout firing";
//             var calcPrimes = function (numMax){
//                 var res = [1, 2];
//                 for (var k = 2; k <= numMax; k++){
//                     var prime = false;
//                     var candidate = res[res.length - 1] + 1;
//                     while(prime === false) {
//                         var good = true;
//                         for (var i = 1; i < res.length; i++) {
//                             if ((candidate % res[i]) === 0) {
//                                 good = false;
//                                 break;
//                             }
//                         }
//                         if (good) {
//                             prime = true;
//                         } else {
//                             candidate++;
//                         }
//                     }
//                     res.push(candidate);
//                 }
//                 return res;
//             }
//             var ress = calcPrimes(1000000);
//             $_____result = ress[ress.length - 1];
//         }, 500);
//     `, &Options{Name: "TestLongTask.js", Timeout: time.Second})
// 	time.Sleep(time.Second * 2)
// 	res, err := w.Run(`return $_____result;`, &Options{Name: "longTaskResult.js"})
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	if res.(string) != "After timeout firing" {
// 		t.Fatal("Script is not terminated")
// 	}
// }

// func TestRequireLibDir(t *testing.T) {
// 	w, err := NewWorker("./libdir", ``)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	res, err := w.Run(`return require("testlib.js");`, &Options{Name: "requireTest.js"})
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	if res.(string) != "EXPORT!!!" {
// 		t.Fatal("Require failed")
// 	}
// }

// type Test struct {
// 	Val string
// }

// func (t *Test) Set(v string) {
// 	t.Val = v
// }

// func (t *Test) Get() string {
// 	return t.Val
// }

// func TestPushGlobalStruct(t *testing.T) {
// 	w, err := NewWorker("./libdir", ``)
// 	if err != nil {
// 		fmt.Println(err)
// 	}

// 	opt := Options{
// 		Name:    "Name",
// 		Timeout: 1 * time.Second,
// 	}

// 	test := Test{}
// 	test.Set("fromGo")

// 	err = w.PushGlobalStruct("test", &test)
// 	if err != nil {
// 		fmt.Println(err)
// 	}
// 	res, err := w.Run("test.set('fromJS'); console.log('Test.V in JS:', test.get())", &opt)

// 	if test.Get() != "fromJS" {
// 		t.Fatal("Struct function call not worked from JS")
// 	}

// 	fmt.Println("test", res, err)
// }
