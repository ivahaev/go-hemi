package path

import (
	"path"
	"strings"

	"bitbucket.org/ivahaev/go-hemi/call"
)

type Path struct{}

func (*Path) Require() string {
	return "path"
}

func (m *Path) Export() interface{} {
	return &Path{}
}

func (*Path) Basename(c *call.Call) string {
	if c.ArgumentsCount() == 0 {
		return ""
	}
	input, err := c.Argument(0).ToString()
	if err != nil || input == "" {
		return ""
	}
	base := path.Base(input)
	if c.ArgumentsCount() == 1 {
		return base
	}
	ext, err := c.Argument(1).ToString()
	if err != nil {
		return base
	}
	return strings.TrimSuffix(base, ext)
}

func (*Path) Dirname(c *call.Call) string {
	if c.ArgumentsCount() == 0 {
		return ""
	}
	input, err := c.Argument(0).ToString()
	if err != nil || input == "" {
		return ""
	}
	return path.Dir(input)
}

func (*Path) Extname(c *call.Call) string {
	if c.ArgumentsCount() == 0 {
		return ""
	}
	input, err := c.Argument(0).ToString()
	if err != nil || input == "" {
		return ""
	}
	return path.Ext(input)
}

func (*Path) IsAbsolute(c *call.Call) bool {
	if c.ArgumentsCount() == 0 {
		return false
	}
	input, err := c.Argument(0).ToString()
	if err != nil || input == "" {
		return false
	}
	return path.IsAbs(input)
}

func (*Path) Join(c *call.Call) string {
	if c.ArgumentsCount() == 0 {
		return ""
	}
	input := []string{}
	for i := 0; i < c.ArgumentsCount(); i++ {
		inputString, err := c.Argument(i).ToString()
		if err != nil || inputString == "" {
			continue
		}
		input = append(input, inputString)
	}
	return path.Join(input...)
}

func (p *Path) Normalize(c *call.Call) string {
	if c.ArgumentsCount() == 0 {
		return ""
	}
	input, err := c.Argument(0).ToString()
	if err != nil || input == "" {
		return ""
	}
	return path.Clean(input)
}

func GetModule() *Path {
	return &Path{}
}
