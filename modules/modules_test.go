package modules

import "testing"

var (
	testPathInputVals = []testId{
		{"test.js", "parent.js", "test.js"},
		{"a/test.js", "parent.js", "a/test.js"},
		{"a/test.js", "parent.js", "a/test.js"},
		{"./a/test.js", "b/parent.js", "b/a/test.js"},
		{"../a/test.js", "b/parent.js", "a/test.js"},
	}
)

type testId struct {
	id       string
	parentId string
	result   string
}

func TestFilePath(t *testing.T) {
	for _, input := range testPathInputVals {
		if input.result != ResolveModuleFilePath(input.id, input.parentId) {
			t.Error("No matched result when getModuleFilePath.", input.result, "!= getModuleFilePath(", input.id, ", ", input.parentId, ")")
		}
	}
}
