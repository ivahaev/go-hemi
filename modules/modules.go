package modules

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"sync"

	"github.com/ivahaev/go-logger"
)

var (
	moduleCache = map[string]string{}
	moduleMutex = &sync.RWMutex{}
)

type packageJson struct {
	Main string `json:"main"`
}

// Get return module script from cache if exists or load from disk
func Get(id string, disableCache bool) (script string, err error) {
	var ok bool
	script, ok = getFromCache(id)
	if ok {
		return script, nil
	}
	script, err = readFile(id)
	if err == nil && !disableCache {
		putToCache(id, script)
	}
	if info, err := os.Stat(id); err == nil {
		if info.IsDir() {
			return "", errors.New("this is a directory")
		}
	}
	return
}

// ResolveModuleFilePath resolve path of id depends of parent id
func ResolveModuleFilePath(id, parentId string) string {
	if strings.HasPrefix(id, "./") || strings.HasPrefix(id, "/") || strings.HasPrefix(id, "../") {
		id = path.Clean(path.Dir(parentId) + "/" + id)
	}
	return id
}

// SearchModule return module path as described in node.js documentation
// https://nodejs.org/api/modules.html#modules_all_together
func SearchModule(id string) (string, error) {
	info, err := os.Stat(id)
	if err == nil {
		if info.IsDir() {
			return resolveModulePathInDir(id)
		}
		return id, nil
	}
	if os.IsExist(err) && !info.IsDir() {
		return id, nil
	}

	newId := id + ".js"
	info, err = os.Stat(newId)
	if err == nil {
		if !info.IsDir() {
			return newId, nil
		}
	}
	if os.IsExist(err) && !info.IsDir() {
		return newId, nil
	}

	newId = id + ".json"
	info, err = os.Stat(newId)
	if err == nil {
		if !info.IsDir() {
			return newId, nil
		}
	}
	if os.IsExist(err) && !info.IsDir() {
		return newId, nil
	}
	return newId, os.ErrNotExist
}

func resolveModulePathInDir(id string) (string, error) {
	path := id + "/package.json"
	if bytes, err := ioutil.ReadFile(path); err == nil {
		var p packageJson
		err = json.Unmarshal(bytes, &p)
		if err != nil {
			logger.Error("Can't unmarshal", err)
			return "", err
		}
		if p.Main == "" {
			return path, errors.New("No main field in " + path)
		}
		return SearchModule(ResolveModuleFilePath("./"+p.Main, path))
	}
	path = id + "/index.js"
	info, err := os.Stat(path)
	if err == nil {
		if !info.IsDir() {
			return path, nil
		}
	}
	if os.IsExist(err) {
		return path, err
	}
	path = id + "/index.json"
	info, err = os.Stat(path)
	if err == nil {
		if !info.IsDir() {
			return path, nil
		}
	}
	if os.IsExist(err) {
		return path, err
	}

	return id, os.ErrNotExist
}

func getFromCache(id string) (string, bool) {
	moduleMutex.RLock()
	script, ok := moduleCache[id]
	moduleMutex.RUnlock()
	return script, ok
}

func putToCache(id, script string) {
	moduleMutex.Lock()
	moduleCache[id] = script
	moduleMutex.Unlock()
}

func readFile(path string) (string, error) {
	if path == "" {
		return "", errors.New("No filepath provided")
	}
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		if os.IsNotExist(err) {
			return "", os.ErrNotExist
		}
		return "", err
	}
	return string(bytes), nil
}
