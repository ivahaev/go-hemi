package timers

import (
	"strconv"
	"sync"
	"time"

	"bitbucket.org/ivahaev/go-hemi/call"
)

var (
	timersSequence int
	timers         = map[int]*time.Timer{}
	timerCallbacks = map[int]func(){}
	timersLocker   sync.Mutex
)

type Timers struct{}

func (*Timers) SetTimeout(cb func(), delay int) interface{} {
	timersLocker.Lock()
	id := timersSequence
	timersSequence++
	timersLocker.Unlock()
	go func() {
		if delay == 0 {
			cb()
			call.RemoveCallback(cb)
			return
		}
		duration := time.Duration(delay) * time.Millisecond
		timer := time.AfterFunc(duration, func() {
			cb()
			call.RemoveCallback(cb)
			timersLocker.Lock()
			delete(timers, id)
			delete(timerCallbacks, id)
			timersLocker.Unlock()
		})
		timersLocker.Lock()
		timers[id] = timer
		timerCallbacks[id] = cb
		timersLocker.Unlock()
	}()
	return strconv.Itoa(id)
}

func (*Timers) ClearTimeout(id int) {
	timersLocker.Lock()
	if timer, ok := timers[id]; ok {
		timer.Stop()
		delete(timers, id)
		if cb, ok := timerCallbacks[id]; ok {
			call.RemoveCallback(cb)
			delete(timerCallbacks, id)
		}
	}
	timersLocker.Unlock()
}
