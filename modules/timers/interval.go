package timers

import (
	"sync"
	"time"

	"bitbucket.org/ivahaev/go-hemi/call"
)

var (
	tickersSequence int
	tickers         = map[int]*time.Ticker{}
	tickersStoppers = map[int]chan struct{}{}
	tickersLocker   sync.Mutex
)

func (*Timers) SetInterval(cb func(), delay int) interface{} {
	tickersLocker.Lock()
	id := tickersSequence
	tickersSequence++
	tickersLocker.Unlock()
	go func() {
		defer call.RemoveCallback(cb)
		if delay == 0 {
			cb()
			return
		}
		duration := time.Duration(delay) * time.Millisecond
		ticker := time.NewTicker(duration)
		tickerStopper := make(chan struct{})
		tickersLocker.Lock()
		tickers[id] = ticker
		tickersStoppers[id] = tickerStopper
		tickersLocker.Unlock()
	selectLoop:
		for {
			select {
			case <-ticker.C:
				go cb()
			case <-tickerStopper:
				break selectLoop
			}
		}
	}()
	return id
}

func (*Timers) ClearInterval(id int) {
	tickersLocker.Lock()
	if ticker, ok := tickers[id]; ok {
		ticker.Stop()
		tickersStoppers[id] <- struct{}{}
		delete(tickers, id)
		delete(tickersStoppers, id)
	}
	tickersLocker.Unlock()
}
