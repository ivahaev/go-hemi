package fs

import (
	"encoding/base64"
	"io/ioutil"
	"os"

	"bitbucket.org/ivahaev/go-hemi/call"
	"github.com/ivahaev/go-logger"
)

var fileReader Reader

type Fs struct{}

type Reader interface {
	ReadFile(string) ([]byte, error)
}

func (*Fs) ReadFile(filePath string, cb func(interface{}, string)) {
	go func() {
		defer call.RemoveCallback(cb)
		var bytes []byte
		var err error

		if fileReader != nil {
			bytes, err = fileReader.ReadFile(filePath)
		} else {
			bytes, err = ioutil.ReadFile(filePath)
		}
		if err != nil {
			logger.Error("Can't read file", err.Error())
			cb("Can't read file", "")
			return
		}
		cb(nil, string(bytes))
	}()
	return
}

func (fs *Fs) ReadFileSync(c *call.Call) interface{} {
	if c.ArgumentsCount() == 0 {
		return nil
	}
	path, err := c.Argument(0).ToString()
	if err != nil {
		logger.Error("Path is not a string", err.Error())
		return nil
	}
	var bytes []byte

	if fileReader != nil {
		bytes, err = fileReader.ReadFile(path)
	} else {
		bytes, err = ioutil.ReadFile(path)
	}
	if err != nil {
		logger.Error("Can't read file", err.Error())
		return nil
	}

	if c.ArgumentsCount() == 1 {
		return string(bytes)
	}

	mod, err := c.Argument(1).ToString()
	if err != nil {
		return string(bytes)
	}
	if mod == "binary" {
		var res = []int{}
		for _, b := range bytes {
			res = append(res, int(b))
		}
		return res
	}

	return string(bytes)
}

func (*Fs) WriteFile(c *call.Call) interface{} {
	if c.ArgumentsCount() < 2 {
		return "Invalid arguments"
	}
	path, err := c.Argument(0).ToString()
	if err != nil {
		logger.Error("Path is not a string", err.Error())
		return "Path is not a string"
	}
	content, err := c.Argument(1).ToString()
	if err != nil {
		logger.Error("Content is not a string", err.Error())
		return "Content is not a string"
	}
	var data []byte
	if c.ArgumentsCount() > 2 {
		encoding, err := c.Argument(2).ToString()
		if err != nil || encoding != "base64" {
			return "Can't detect encoding: " + err.Error()
		}
		data, err = base64.StdEncoding.DecodeString(content)
		if err != nil {
			return "Can't decode content: " + err.Error()
		}
	} else {
		data = []byte(content)
	}

	err = ioutil.WriteFile(path, data, 0644)
	if err != nil {
		logger.Error("Can't read file", err.Error())
		return "Can't read file " + err.Error()
	}

	return nil
}

func (*Fs) Unlink(filePath string, cb func(interface{})) {
	err := os.Remove(filePath)
	if err != nil {
		cb(err.Error())
		return
	}
	cb(nil)
}

func (*Fs) UnlinkSync(filePath string) interface{} {
	err := os.Remove(filePath)
	if err != nil {
		return err.Error()
	}
	return nil
}

func SetReader(r Reader) {
	fileReader = r
}

func (fs *Fs) WriteFileSync(c *call.Call) interface{} {
	return fs.WriteFile(c)
}

func (m *Fs) Require() string {
	return "fs"
}

func (m *Fs) Export() interface{} {
	return &Fs{}
}

func GetModule() *Fs {
	return &Fs{}
}
