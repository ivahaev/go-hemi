package hemi

import "strings"

var (
	jsRequire = `
        'use strict';

        var $sequence = 0;
        this.__callNativeCommand = $send;
        this.__callNativeCommandSync = $sendSync;

        //Seeting handler for messages from Golang
        $recv(__nativeMessageHandler);

        //Creating cache for modules and callbacks
        if (typeof $requiredModules === 'undefined') {
            Object.defineProperty(this, '$requiredModules', {
                value: {}, writable: true, enumerable: false, configurable: true
            });
        }
        if (typeof $callbacks === 'undefined') {
            Object.defineProperty(this, '$callbacks', {
                value: {}, writable: true, enumerable: false, configurable: true
            });
        }

        function __nativeMessageHandler (msg) {
            var message;
            try {
                message = JSON.parse(msg);
            } catch (e) {
                //console.error("Error when receiving message: ", e, " message:", msg);
                return;
            }
            switch (message.command) {
                case "$pushGlobalInterface":
                    var value = message.arguments[1];
                    if (message.module == message.arguments[0]) {
                        value = $registerModule(message.module, value);
                    }
                    Object.defineProperty(this, message.arguments[0], {
                        value: value, writable: true, enumerable: true, configurable: true
                    });
                    break;
                case "$cb.remove":
                    $cb.remove(message.response);
                    break;
                case "$cb.call":
                    $cb.call(message.response, message.arguments);
                    if (message["$cb.remove"] === true) {
                        $cb.remove(message.response);
                    }
                    break;
                default:
                    if (message.sequence && $callbacks[message.sequence]) {
                        var cb = $callbacks[message.sequence];
                        if (Array.isArray(message.response)) {
                        cb.callback.apply(cb.context, message.response);
                        } else {
                            cb.callback.apply(cb.context, [message.response]);
                        }
                        delete $callbacks[message.sequence];
                    }
                    break;
            }
        }

        function $requireModule (parentId, id) {
            if (id == null) {
                throw new Error("No id provided")
            }

            id = $resolveModuleFilePath(id, parentId);
            if (id == "") {
                throw new Error("No id provided")
            }

            if ($requiredModules[id]) {
                return $requiredModules[id]
            }
            var command = {
                "command": "$moduleRequire",
                "module": id
            };

            var result = __callNativeCommandSync(JSON.stringify(command));
            if (result === "") {
                throw new Error("Unknown error");
            }
            var module;
            try {
                module = JSON.parse(result);
            } catch (e) {
                return $requiredModules[id]
            }

            if (module.error) {
                var newId;
                if (module.error === "file does not exist") {
                    newId = $searchModule(id);
                    if (newId !== "") {
                        return $requireModule("", newId);
                    }
                    newId = $searchModule("local/lib/" + id);
                    if (newId !== "") {
                        return $requireModule("", newId);
                    }
                    newId = $searchModule("lib/" + id);
                    if (newId !== "") {
                        return $requireModule("", newId);
                    }
                    newId = $searchModule("vendor/" + id);
                    if (newId !== "") {
                        return $requireModule("", newId);
                    }

                    newId = $searchModule("node_modules/" + id);
                    if (newId !== "") {
                        return $requireModule("", newId);
                    }
                }
                if (module.error === "this is a directory") {
                    newId = $searchModule(id);
                    return $requireModule("", newId);
                }
                throw new Error("Module '" + id + "' load error: " + module.error);
            }

            if (!module.$ThisIsEmbeddedModule) {
                return $requiredModules[id]
            }

            module = $registerModule(id, module);
            $requiredModules[id] = module;

            return module
        }

        function callNativeCommand (moduleId, command) {
            var args = Array.prototype.slice.call(arguments, 2);
            for (var i = 0; i < args.length; i++) {
                if (typeof args[i] === "function") {
                    var cbId = $cb.add(args[i]);
                    args[i] = "$callMePlease:" + cbId;
                }
            }
            command = {
                "module": moduleId,
                "command": command,
                "arguments": args
            };

            var callResult = __callNativeCommandSync(JSON.stringify(command));
            var response = JSON.parse(callResult);
            if (response.error) {
                throw new Error(response.error);
            }
            return response.response;
        }

        function $registerModule(id, module) {
            var methods = module.methods;
            if (methods.length === 0) {
                module = module.props
                $requiredModules[id] = module;
                return module
            }
            module = module.props;
            for (var i = 0; i < methods.length; i++) {
                module[methods[i]] = callNativeCommand.bind(this, id, methods[i])
            }

            return module
        }

        function $resolveModuleFilePath(id, parentId) {
            var command = {
                "command": "$resolveModuleFilePath",
                "arguments": [id, parentId]
            };

            return __callNativeCommandSync(JSON.stringify(command));
        }

        function $searchModule(id, parentId) {
            var command = {
                "command": "$searchModule",
                "arguments": [id, parentId]
            };

            return __callNativeCommandSync(JSON.stringify(command));
        }
    `

	jsCb = `
        //Store and helper for async callbacks calling from Go code

        var global = this;
        (function () {
            var id = 0, prefix = "", callbacks = {};

            function nextId() {
                var res = id;
                if (id >= Number.MAX_SAFE_INTEGER) {
                    id = 0;
                    prefix += "A";
                } else {
                    id++;
                }
                return prefix + res;
            }

            function __addCallback(fn) {
                if (typeof fn !== 'function') {
                    throw new Error("Invalid argument: fn. Must be a function");
                }
                var fnId = nextId();
                callbacks[fnId] = fn;
                return fnId;
            }

            function __deleteCallback(fn) {
                if (typeof fn !== 'function') {
                    __deleteCallbackById(fn);
                    return;
                }
                var fnId = null;
                for (var id in callbacks) {
                    if (callbacks[id] === fn) {
                        fnId = id;
                        break;
                    }
                }
                if (fnId !== null) {
                    delete callbacks[fnId];
                }
            }

            function __deleteCallbackById(fnId) {
                if (!callbacks.hasOwnProperty(fnId)) {
                    throw new Error("Can not unregister callback with id: '" + fnId + "' - callback not found.");
                }
                delete callbacks[fnId];
            }

            function __runCallback(fnId, args) {
                if (!callbacks.hasOwnProperty(fnId)) {
                    throw new Error("Can not run callback with id: '" + fnId + "' - callback not found.");
                }
                callbacks[fnId].apply(this, args);
            }

            //Public API
            global.$cb = {
                "add": __addCallback,
                "remove": __deleteCallback,
                "call": __runCallback
            }
        })();
`

	consolePolyfill = `
        if (typeof console === 'undefined') {
            Object.defineProperty(this, 'console', {
                value: {}, writable: true, enumerable: false, configurable: true
            });
        }
        if (typeof console.log === 'undefined') {
            (function () {
                var origPrint = $print;  // capture in closure in case changed later
                Object.defineProperty(this.console, 'debug', {
                    value: function () {
                        var getStack = function() {
                            var data = {}
                            var stackReg = /at\s+(.*)\s+\((.*):(\d*):(\d*)\)/i;
                            var stackReg2 = /at\s+()(.*):(\d*):(\d*)/i;
                            var stacklist = (new Error()).stack.split('\n').slice(3);
                            var s = stacklist[0],
                                    sp = stackReg.exec(s) || stackReg2.exec(s);
                            if (sp && sp.length === 5) {
                                    data.method = sp[1];
                                    data.file = sp[2];
                                    data.line = sp[3];
                                    data.pos = sp[4];
                            }
                                return data
                        }
                        var strArgs = Array.prototype.map.call(arguments, function (v) {
                            if (typeof v === "object") {
                                return JSON.stringify(v, null, "\t");
                            }
                            return String(v);
                        });
                        var stack = getStack();
                        var output = "[DEBUG] " + stack.file + ":" + stack.line + ":" + stack.pos + " " + (stack.method ? stack.method + "()\t" : "\t") + Array.prototype.join.call(strArgs, ' ');
                        origPrint(output);
                    }, writable: false, enumerable: false, configurable: false
                });
                Object.defineProperty(this.console, 'log', {
                    value: function () {
                        var strArgs = Array.prototype.map.call(arguments, function (v) { return String(v); });
                        origPrint(Array.prototype.join.call(strArgs, ' '));
                    }, writable: false, enumerable: false, configurable: false
                });
                Object.defineProperty(this.console, 'info', {
                    value: function () {
                        var strArgs = Array.prototype.map.call(arguments, function (v) { return String(v); });
                        origPrint('\x1b[36m' + Array.prototype.join.call(strArgs, ' ') + '\x1b[0m');
                    }, writable: false, enumerable: false, configurable: false
                });
                Object.defineProperty(this.console, 'error', {
                    value: function () {
                        var strArgs = Array.prototype.map.call(arguments, function (v) { return String(v); });
                        origPrint('\x1b[31m' + Array.prototype.join.call(strArgs, ' ') + '\x1b[0m');
                    }, writable: false, enumerable: false, configurable: false
                });
            })();
        }
    `
)

func nameToJavaScript(name string) string {
	var toLower, keep string
	for _, c := range name {
		if c >= 'A' && c <= 'Z' && len(keep) == 0 {
			toLower += string(c)
		} else {
			keep += string(c)
		}
	}

	lc := len(toLower)
	if lc > 1 && lc != len(name) {
		keep = toLower[lc-1:] + keep
		toLower = toLower[:lc-1]

	}

	return strings.ToLower(toLower) + keep
}
