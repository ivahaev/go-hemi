package tester

import (
	"bitbucket.org/ivahaev/go-hemi"
	"bitbucket.org/ivahaev/go-hemi/call"
	"github.com/ivahaev/go-logger"
)

type T struct {
	StringProp string
	IntProp    int
	unexported bool
}

func (*T) Info(c *call.Call) {
	if c.ArgumentsCount() == 0 {
		return
	}
	str, err := c.Argument(0).ToString()
	if err != nil {
		return
	}
	logger.Info("Print()", str)
}

func (*T) Debug(c *call.Call) {
	if c.ArgumentsCount() == 0 {
		return
	}
	str, err := c.Argument(0).ToString()
	if err != nil {
		return
	}
	logger.Debug("Debug()", str)
}

func (*T) Require() string {
	return "test"
}

func (t *T) Export() interface{} {
	return t
}

func (*T) InitScript() string {
	return ""
}

func init() {
	hemi.RegisterRequire(&T{})
}
