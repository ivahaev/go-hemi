//Store and helper for async callbacks calling from Go code

var global = this;
(function () {
    var id = 0, prefix = "", callbacks = {};

    function nextId() {
        var res = id;
        if (id >= Number.MAX_SAFE_INTEGER) {
            id = 0;
            prefix += "A";
        } else {
            id++;
        }
        return prefix + res;
    }

    function __addCallback(fn) {
        if (typeof fn !== 'function') {
            throw new Error("Invalid argument: fn. Must be a function");
        }
        var fnId = nextId();
        callbacks[fnId] = fn;
        return fnId;
    }

    function __deleteCallback(fn) {
        if (typeof fn !== 'function') {
            __deleteCallbackById(fn);
            return;
        }
        var fnId = null;
        for (var id in callbacks) {
            if (callbacks[id] === fn) {
                fnId = id;
                break;
            }
        }
        if (fnId !== null) {
            delete callbacks[fnId];
        }
    }

    function __deleteCallbackById(fnId) {
        if (!callbacks.hasOwnProperty(fnId)) {
            throw new Error("Can not unregister callback with id: '" + fnId + "' - callback not found.");
        }
        delete callbacks[fnId];
    }

    function __runCallback(fnId, args) {
        if (!callbacks.hasOwnProperty(fnId)) {
            throw new Error("Can not run callback with id: '" + fnId + "' - callback not found.");
        }
        callbacks[fnId].apply(this, args);
    }

    //Public API
    global.$cb = {
        "add": __addCallback,
        "remove": __deleteCallback,
        "call": __runCallback
    }
})();